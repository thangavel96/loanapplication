package com.finance.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.finance.constants.DataConstants;
import com.finance.exception.ServiceResponseError;
import com.finance.model.Customer;
import com.finance.model.Response;
import com.finance.model.Status;
import com.finance.services.CustomerService;
import com.finance.services.DataValidator;


@RestController
public class CustomerController {

	@Autowired
	CustomerService service;

	Response response;

	@GetMapping(path = "getCustomer/{id}")
	public Response getCustomer(@PathVariable("id")String custId ) {

		try {
			DataValidator.validateCustomerId(custId);
			response=new Response();
			Customer customer =service.getCustomerById(custId);
			response.setData(customer); 
		} catch (Exception e) {
			String errorCode=DataConstants.errorCode101;
			String msg=e.getMessage();
			if( e instanceof ServiceResponseError) {
				errorCode=((ServiceResponseError) e).getStatusCode();
				msg=((ServiceResponseError) e).getMsg();
			}
			response= service.getDefaultResponse(errorCode,msg);
		}
		return response;
	}

	@GetMapping(path = "getCustomers")
	public Response getAllCustomer() {

		try {
			response=new Response();
			List<Customer> customerList = service.getAllCustomers();
			response.setData(customerList); 
		} catch (Exception e) {
			String errorCode=DataConstants.errorCode101;
			String msg=e.getMessage();
			if( e instanceof ServiceResponseError) {
				errorCode=((ServiceResponseError) e).getStatusCode();
				msg=((ServiceResponseError) e).getMsg();
			}
			response= service.getDefaultResponse(errorCode,msg);
		}finally {
			setStatus();
		}
		return response;
	}

	@PostMapping(path = "newCustomer",produces = "application/json", consumes = "application/json")
	public Response insertCustomer(@RequestBody Customer customer) {

		try {
			DataValidator.validateCustomerId(customer);
			response=new Response();
			response.setData(service.insertCustomer(customer));
		} catch (Exception e) {
			String errorCode=DataConstants.errorCode101;
			String msg=e.getMessage();
			if( e instanceof ServiceResponseError) {
				errorCode=((ServiceResponseError) e).getStatusCode();
				msg=((ServiceResponseError) e).getMsg();
			}
			response= service.getDefaultResponse(errorCode,msg);
		}

		return response;
	}
	
	public void setStatus() {
		System.out.println(response);
		Status status =new Status();
		if(null != response) {
		}
	}


}
