package com.finance.exception;

public class ServiceResponseError  extends Exception{

	
	private static final long serialVersionUID = 1L;
	
	private String statusCode;
	private String msg;
	
	public ServiceResponseError(String statusCode, String msg) {
		super();
		this.statusCode = statusCode;
		this.msg = msg;
	}
	
	public String getStatusCode() {
		return statusCode;
	}
	
	public String getMsg() {
		return msg;
	}
	

}
