package com.finance.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Component;

import com.finance.model.Customer;

@Component
public class DataUtil {
	
	List<Customer> customerList;
	
	public DataUtil() {
		System.out.println("Data util invoked...");
		customerList=new ArrayList<Customer>(Arrays.asList(new Customer("cus01","Pavithran"),new Customer("cus02","Thangave")));
	}
	
	public List<Customer> getAllCustomers() {
		if(null == customerList)
			return new ArrayList<Customer>();
		return customerList;
	}

	public Customer getCustomerById(String custId) {
		if(null != customerList && !customerList.isEmpty())
			return customerList.stream()
					.filter(cus -> custId.equals(cus.getCustId()))
					.findFirst()
					.orElse(null);
		
		return null;
	}

	public Customer insertCustomer(Customer customer) {
		if(null != customerList)
			customerList.add(customer);
		return customer;
	}

}
