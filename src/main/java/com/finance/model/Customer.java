package com.finance.model;

public class Customer {

	
	
	public Customer(String id,String name) {
		this.custId = id;
		this.name = name;
	}
	
	
	public Customer() {
	}


	private String custId;
	private String name;
	
	
	public String getCustId() {
		return custId;
	}
	public void setCustId(String custId) {
		this.custId = custId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	

}
