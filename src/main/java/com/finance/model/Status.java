package com.finance.model;

public class Status {
	
	private String code;
	private String msg;
	public String getStatusCode() {
		return code;
	}
	public void setStatusCode(String statusCode) {
		this.code = statusCode;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	
	

}
