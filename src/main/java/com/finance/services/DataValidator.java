package com.finance.services;

import com.finance.model.Customer;

public class DataValidator {
	
	private static final String CUSTOMERID = " Customer ID is ";
	private static final String CUSTOMER = " Customer is ";
	private static final String EMPTY ="empty/null";

	public static void validateCustomerId(String custId) throws Exception{
		if(null == custId)
			throw new Exception(CUSTOMERID+EMPTY);
		
	}

	public static void validateCustomerId(Customer customer)  throws Exception{
		if(null == customer || null == customer.getName())
			throw new Exception(CUSTOMER+EMPTY);

	}

}
