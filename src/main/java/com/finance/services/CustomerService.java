package com.finance.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.finance.exception.ServiceResponseError;
import com.finance.model.Customer;
import com.finance.model.Error;
import com.finance.model.Response;
import com.finance.utils.DataUtil;

@Component
public class CustomerService {
	
	@Autowired
	DataUtil dataUtil;

	public Customer getCustomerById(String custId) throws Exception {
		Customer customer=dataUtil.getCustomerById(custId);
		if(null == customer )
			throw new ServiceResponseError("101","test");
		
		return customer;
	}
	
	public List<Customer> getAllCustomers() {
		return dataUtil.getAllCustomers();
	}
	

	public Customer insertCustomer(Customer customer) {
		int totalCustomer=getAllCustomers().size();
		String custId="cus"+(getAllCustomers().size()>9?totalCustomer:"0"+(totalCustomer+1));
		
		customer.setCustId(custId);
		
		return dataUtil.insertCustomer(customer);
		
	}

	public Response getDefaultResponse(String statusCode, String errorMsg) {
		Response defaultRes = new Response();
		List<Error> errorList =new ArrayList<Error>();
		Error error =new Error();
		error.setCode(statusCode);
		error.setDetails(errorMsg);
		errorList.add(error);
		defaultRes.setErrors(errorList);
		return defaultRes;
		
	}

	

}
